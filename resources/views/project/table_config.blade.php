@extends('app')

@section('content')

    <div class="container">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            {!! Form::open(['route' => 'project.store.fields', 'method' => 'post']) !!}
            <input type="hidden" name="id" value="{{ $id }}">
            <input type="submit" value="enviar">

            <?php $cnt=0;?>
            @foreach($tables as $key=>$collaplse)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{ $cnt }}">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $cnt }}" aria-expanded="true" aria-controls="collapse{{ $cnt }}">
                                {{ $key }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $cnt }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $cnt }}">
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Coluna</th>
                                        <th>Form</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($collaplse['columns'] as $columns)
                                    @if($columns != 'id' && $columns != 'created_at' && $columns != 'updated_at')

                                        <tr>
                                            <td>{{ $columns }}</td>
                                            <td><input type="checkbox" name="form[{{$key}}][{{ $columns }}][]" value="1"></td>
                                        </tr>

                                    @endif
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php $cnt++;?>
            @endforeach
            {!! Form::close() !!}
        </div>
    </div>


@endsection