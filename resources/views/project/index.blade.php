@extends('app')

@section('content')
    <div class="container">
        <h3>Nova Projeto</h3>

        {!! Form::open(['route' => 'project.store']) !!}

        @include('project._form')

        <div class="form-group">
            {!! Form::submit('Criar Categoria', ['class'=>'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>

@endsection