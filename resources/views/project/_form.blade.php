
<div class="form-group">
    {!! Form::label('Name', 'Nome do Project:') !!}
    {!! Form::text('name_project', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Host', 'Host do Banco de Dados:') !!}
    {!! Form::text('config[host]', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dbname', 'Nome do Banco de Dados:') !!}
    {!! Form::text('config[dbname]', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('User', 'User do Banco de Dados:') !!}
    {!! Form::text('config[user]', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('Password', 'Senha do Banco de Dados:') !!}
    {!! Form::text('config[password]', null, ['class' => 'form-control']) !!}
</div>