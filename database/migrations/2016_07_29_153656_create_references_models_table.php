<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencesModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_reference');
            $table->string('model_reference');
            $table->integer('columns_tables_id')->unsigned();
            $table->foreign('columns_tables_id')->references('id')->on('columns_tables');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('references_models');
    }
}
