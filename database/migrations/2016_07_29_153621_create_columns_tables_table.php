<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_form');
            $table->string('type_field');
            $table->string('type_data');
            $table->boolean('required');
            $table->integer('table_project_id')->unsigned();
            $table->foreign('table_project_id')->references('id')->on('table_projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('columns_tables');
    }
}
