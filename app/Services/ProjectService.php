<?php
/**
 * Created by PhpStorm.
 * User: DevMaker 2016
 * Date: 28/07/2016
 * Time: 16:37
 */

namespace App\Services;


use App\ConfigProject;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProjectController;
use App\Project;
use App\TableProject;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ProjectService extends Controller
{

    /**
     * @var Project
     */
    private $project;
    /**
     * @var TableProject
     */
    private $tableProject;
    /**
     * @var ConfigProject
     */
    private $configProject;
    /**
     * @var ProjectController
     */
    private $projectController;

    public function __construct(Project $project, TableProject $tableProject, ConfigProject $configProject)
    {
        $this->project = $project;
        $this->tableProject = $tableProject;
        $this->configProject = $configProject;
    }

    public function store(array $data)
    {
        $project = $this->project->create($data);
        $data['config']['project_id'] = $project->id;
        $this->configProject->create($data['config']);

        return $project->id;
    }

    public function showtable($id)
    {
        $project = \App\Project::find($id);
        //$pdo = $this->pdo($project->config->host, $project->config->dbname, $project->config->user, $project->config->password);

        Config::set('database.connections.db', array(
            'driver'    => 'mysql',
            'host'      => $project->config->host,
            'database'  => $project->config->dbname,
            'username'  => $project->config->user,
            'password'  => $project->config->password,
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
            'fetch' => \PDO::FETCH_ASSOC,
        ));

        $tables = DB::connection('db')->select('SHOW TABLES');

        $tables = array_map(function($item){
            return (array) $item;
        },$tables);

        $content = [];

        foreach ($tables as $table) {
            $key = 'Tables_in_'.$project->config->dbname;

            $data_table = [];
            $data_table['project_id'] =  $project->id;
            $data_table['table_name'] =  $table[$key];

            $table_obj = \App\TableProject::create($data_table);

            $columns = DB::connection('db')->select('SHOW COLUMNS FROM '.$table[$key]);

            $columns = array_map(function($item){
                return (array) $item;
            },$columns);

            for($i=0; $i<count($columns); $i++){
                if($columns[$i]['Field'] != 'id' && $columns[$i]['Field'] != 'created_at' && $columns[$i]['Field'] !='updated_at'){
                    $column_data['name'] = $columns[$i]['Field'];
                    $column_data['table_project_id'] = $table_obj->id;
                    $column_data['type_data'] = $columns[$i]['Type'];

                    \App\ColumnsTable::create($column_data);
                }

                $content[$table[$key]]['columns'][$i] = $columns[$i]['Field'];
            }
        }

        return $content;
    }


}