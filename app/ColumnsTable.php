<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColumnsTable extends Model
{
    protected $fillable = [
        'name',
        'type_field',
        'type_data',
        'required',
        'table_project_id'
    ];
}
