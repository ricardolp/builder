<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableProject extends Model
{
    protected $fillable = [
        'table_name',
        'project_id'
    ];

    public function column()
    {
        return $this->hasMany('\App\ColumnsTable');
    }
}
