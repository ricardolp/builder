<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ProjectController@index');
Route::post("project/store", ['as' => 'project.store', 'uses'=>'ProjectController@store']);
Route::get("project/tables/{id}", ['as' => 'project.tables', 'uses'=>'ProjectController@tables']);
Route::post("project/store/fields", ['as' => 'project.store.fields', 'uses'=>'ProjectController@fields']);
Route::get("project/generate/{id}", ['as' => 'project.generate', 'uses'=>'ProjectController@generate']);
