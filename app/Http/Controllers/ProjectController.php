<?php

namespace App\Http\Controllers;

use App\Services\ProjectService;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function index()
    {
        return view("project.index");
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $project = $this->projectService->store($data);

        return redirect()->route('project.tables', $project);
    }

    public function tables($id)
    {
        $tables = $this->projectService->showtable($id);
        return view('project.table_config', compact('tables', 'id'));
    }

    public function fields(Request $request)
    {
        $data = $request->all();

        foreach ($data['form'] as $key=>$item) {
            $table = \App\TableProject::where('project_id', $data['id'])->where('table_name', $key)->get();

            $table_id = $table[0]->id;

            foreach ($item as $key=>$columns) {
                $column_data = \App\ColumnsTable::where('table_project_id', $table_id)->where('name', $key)->get();
                $column_data[0]->is_form = 1;
                $column_data[0]->save();
            }

        }

        return redirect()->route('project.generate', $data['id']);
    }

    public function generate($id)
    {
        $generator_code = '';
        $field_code = '';

        $project = \App\Project::find($id);

        $bat = 'cd '.base_path('public\projects') . ' && composer create-project --prefer-dist laravel/laravel '.$project->name." \n";
        $bat .= "composer require appzcoder/crud-generator \n";
        $bat .= "composer require laravelcollective/html \n";
        $bat .= '@echo off
                    setlocal enabledelayedexpansion
                    set /a counter=0
                    set /a %%a = ""
                    for /f "usebackq delims=" %%a in ('.base_path('config\app.php') . ') do (
                       if "!counter!"=="%155" goto :printme & set /a counter+=1
                    )
                    :printme
                    echo %%a';

        foreach ($project->table as $entity) {

            $text =  preg_replace_callback('/(?:^|_)([a-z])/',
                function ($m) {
                    return strtoupper($m[1]);
                }, $entity->table_name);

            $table = \App\TableProject::where('project_id', $id)->where('table_name', $entity->table_name)->get()->first();

            foreach ($table->column as $column){
                ($table->column->last()->name != $column->name) ? $v=',' : '';
                $field_code .= $column->name . "#" . $this->getType($column->type_data) . $v;
            }

            $bat .= "php artisan crud:generate $text --fields='$field_code' --route=yes --pk=id --view-path='admin' --route-group=admin \n";

        }

        file_put_contents(base_path('public/teste.txt'), $bat);
    }

    public function getType($sentence)
    {
        $type = explode('(', $sentence);
        if(count($type) > 0){
            return $type[0];
        } else {
            return $sentence;
        }
    }
}
