<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name_project',
        'output_path'
    ];

    public function config(){
        return $this->hasOne('\App\ConfigProject');
    }

    public function table()
    {
        return $this->hasMany('\App\TableProject');
    }
}
