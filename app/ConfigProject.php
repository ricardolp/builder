<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigProject extends Model
{
    protected $fillable = [
        'host',
        'user',
        'password',
        'dbname',
        'project_id'
    ];
}
